package com.example.bluetry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.Toast;

import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    String msg = "Android : ";
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
            .getDefaultAdapter();
    private BluetoothDevice targetDevice;
    private BluetoothLeScanner bluetoothLeScanner;
    private List<BluetoothGattService> mServiceList;
    private UUID targetUUID = UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");

    /** 当活动第一次被创建时调用 */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(msg, "The onCreate() event");
        if(mBluetoothAdapter == null){
            Toast.makeText(this, "不支持蓝牙", Toast.LENGTH_SHORT).show();
        }else{
            enableBT();
        }
    }

    // method enable Bluetooth
    protected void enableBT(){
        // case: bluetooth already enabled
        if(mBluetoothAdapter.isEnabled()){
            Log.d(msg, "蓝牙已经提前开启");
            Toast.makeText(this, "蓝牙已经提前开启", Toast.LENGTH_SHORT).show();
        }else{
            // case: bluetooth not enabled yet
            Toast.makeText(this, "正在开启蓝牙", Toast.LENGTH_SHORT).show();
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 1);
        }

        // start advertising

        // start scanning
        Log.d(msg, "蓝牙开始扫描");
        scanLeDevice();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "蓝牙已经开启", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "没有蓝牙权限", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    // setup and start scan BT_LE device
    private void scanLeDevice() {
        // ask user for COARSE_LOCATION Permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},1);
        }
        Log.d(msg, "蓝牙正在扫描");
        // setup scanner then start scan
        bluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        bluetoothLeScanner.startScan(callback);
    }

    private ScanCallback callback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            List<ParcelUuid> uuids = result.getScanRecord().getServiceUuids();
            if(result.getDevice().getName() != null){
                Log.d(msg, "扫描的设备:"+result.getDevice().getName());
                result.getDevice().connectGatt(MainActivity.this, false, mGattCallback);
            }
            super.onScanResult(callbackType, result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
        }
    };

    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        /**
         * Callback indicating when GATT client has connected/disconnected to/from a remote GATT server
         *
         * @param gatt 返回连接建立的gatt对象
         * @param status 返回的是此次gatt操作的结果，成功了返回0
         * @param newState 每次client连接或断开连接状态变化，STATE_CONNECTED 0，STATE_CONNECTING 1,STATE_DISCONNECTED 2,STATE_DISCONNECTING 3
         */
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.d(msg, "onConnectionStateChange status:" + status + "  newState:" + newState);
            if (status == 0) {
                gatt.discoverServices();
            }
        }

        /**
         * Callback invoked when the list of remote services, characteristics and descriptors for the remote device have been updated, ie new services have been discovered.
         *
         * @param gatt 返回的是本次连接的gatt对象
         * @param status
         */
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Log.d(msg, "onServicesDiscovered status" + status);
            mServiceList = gatt.getServices();

            if (mServiceList != null) {
                //System.out.println(mServiceList);
                System.out.println("Services num:" + mServiceList.size());
            }else{
                Log.d(msg, "扫描的服务为NULL");
            }

            for (BluetoothGattService service : mServiceList){
                List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                System.out.println("扫描到Service：" + service.getUuid());

                for (BluetoothGattCharacteristic characteristic : characteristics) {
                    //gatt.setCharacteristicNotification(characteristic, true);
                    //gatt.readCharacteristic(characteristic);
                    System.out.println("characteristic: " + characteristic.getUuid() );
                }
            }
        }

        /**
         * Callback triggered as a result of a remote characteristic notification.
         * Notification !!!!!!!!!
         * @param gatt
         * @param characteristic
         */
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Log.d(msg, "onCharacteristicChanged");
            System.out.println("characteristic: " + characteristic.getUuid() );
            System.out.println("characteristic: " + characteristic.getUuid() );
            System.out.println("characteristic: " + characteristic.getUuid() );
            System.out.println("characteristic: " + characteristic.getUuid() );
            System.out.println("characteristic: " + characteristic.getUuid() );
            System.out.println("characteristic: " + characteristic.getUuid() );
            System.out.println("characteristic: " + characteristic.getUuid() );
            System.out.println("测试字符: " + characteristic.getValue() );
            Log.d(msg, "###########");

        }

        /**
         *Callback reporting the result of a characteristic read operation.
         *
         * @param gatt
         * @param characteristic
         * @param status
         */
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.d(msg, "onCharacteristicRead");
//            gatt.readCharacteristic(characteristic);
            Log.d(msg,"characteristic: " + characteristic.getUuid());
            Log.d(msg,"characteristic: " + characteristic.getUuid());
            Log.d(msg,"characteristic: " + characteristic.getUuid());
            Log.d(msg,"characteristic: " + characteristic.getUuid());
            Log.d(msg,"characteristic: " + characteristic.getUuid());
            Log.d(msg,"characteristic: " + characteristic.getUuid());
            Log.d(msg, "测试字符"+characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32,0));

        }

    };

    /** 当活动即将可见时调用 */
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(msg, "The onStart() event");
    }

    /** 当活动可见时调用 */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(msg, "The onResume() event");
    }

    /** 当其他活动获得焦点时调用 */
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(msg, "The onPause() event");
    }

    /** 当活动不再可见时调用 */
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(msg, "The onStop() event");
    }

    /** 当活动将被销毁时调用 */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(msg, "The onDestroy() event");
    }
}
